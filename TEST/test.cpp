#include "estados.h"
#include <gtest/gtest.h>

TEST(SquareRootTest, PositiveNos){

	/*Estados estadoSig;
	estadoSig  = goNORTE;*/
	//int estadoactual = goNORTE;
    ASSERT_EQ(goNORTE, selectState(NO_CARRO,goNORTE));
    ASSERT_EQ(goNORTE, selectState(CARROS_NORTE,goNORTE));
    ASSERT_EQ(waitNORTE, selectState(CARROS_ESTE,goNORTE));
    ASSERT_EQ(waitNORTE, selectState(AMBOS_LADOS,goNORTE));

    ASSERT_EQ(goESTE, selectState(NO_CARRO,waitNORTE));
    ASSERT_EQ(goESTE, selectState(CARROS_ESTE,waitNORTE));
    ASSERT_EQ(goESTE, selectState(CARROS_NORTE,waitNORTE));
    ASSERT_EQ(goESTE, selectState(AMBOS_LADOS,waitNORTE));

	ASSERT_EQ(goESTE, selectState(NO_CARRO,goESTE));
	ASSERT_EQ(goESTE, selectState(CARROS_ESTE,goESTE));    
	ASSERT_EQ(waitESTE, selectState(CARROS_NORTE,goESTE));
	ASSERT_EQ(waitESTE, selectState(AMBOS_LADOS,goESTE));

	ASSERT_EQ(goNORTE, selectState(NO_CARRO,waitESTE));
	ASSERT_EQ(goNORTE, selectState(CARROS_ESTE,waitESTE));
	ASSERT_EQ(goNORTE, selectState(CARROS_NORTE,waitESTE));
	ASSERT_EQ(goNORTE, selectState(AMBOS_LADOS,waitESTE));
    /*ASSERT_EQ(18.0, selectState(324.0));
    ASSERT_EQ(25.4, selectState(645.16));
    ASSERT_EQ(0, selectState(0.0));*/
}

/*TEST(SquareRootTest, NegativeNos){
    ASSERT_EQ(0, selectState(0,0));
   // ASSERT_EQ(-1.0, squareRoot(-0.2));
}*/

int main(int argc, char **argv){
    testing::InitGoogleTest(&argc, argv);
    return RUN_ALL_TESTS();
}