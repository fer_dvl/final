#ifndef __ESTADOS_H
#define __ESTADOS_H


#ifdef __cplusplus
extern "C"{
#endif


#include <stdio.h>
#include <stdlib.h>

typedef enum{
	goNORTE,
	waitNORTE,
	goESTE,
	waitESTE
} Estados;

typedef enum{
	NO_CARRO,
	CARROS_ESTE,
	CARROS_NORTE,
	AMBOS_LADOS
} carros;
//int estadoactual;

int selectState(int carro, int estado2);
void printStates(int estado);

#ifdef __cplusplus
}
#endif

#endif /* __FSM_H */


